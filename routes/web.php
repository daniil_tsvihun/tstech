<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('customer', 'CustomerController',
    ['only' => ['index', 'show']]);
Route::get('account/commission', 'AccountController@commission');
Route::get('account/capitalize', 'AccountController@capitalize');