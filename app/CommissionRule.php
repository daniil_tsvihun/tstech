<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommissionRule extends Model
{
    const TABLE_NAME='commission_rules';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table=$this::TABLE_NAME;
    }

    /**
     * Calculation of the deposit commission for the given value
     * @param $balance
     * @return mixed
     */
    public static function calcCommission($balance)
    {
        /* ***RAW QUERY***

                SELECT
                  CASE
                    WHEN cr.min_commission>ROUND($balance*cr.percentage/100,2) THEN cr.min_commission
                    WHEN cr.max_commission IS NOT NULL AND cr.max_commission<ROUND($balance*cr.percentage/100,2) THEN cr.max_commission
                    ELSE ROUND($balance*cr.percentage/100,2)
                  END
                FROM commission_rules cr
                WHERE value_from<=$balance AND (value_to>=$balance OR value_to IS NULL)

        */
        return CommissionRule::selectRaw('
                CASE
                    WHEN 
                      commission_rules.min_commission>ROUND('.$balance.'*commission_rules.percentage/100,2) 
                        THEN commission_rules.min_commission
                    WHEN 
                      commission_rules.max_commission IS NOT NULL 
                      AND commission_rules.max_commission<ROUND('.$balance.'*commission_rules.percentage/100,2) 
                        THEN commission_rules.max_commission
                    ELSE ROUND('.$balance.'*commission_rules.percentage/100,2)
                END
                ')
            // matches commission conditions
            ->whereRaw('
            commission_rules.value_from<='.$balance.' 
                AND (commission_rules.value_to>'.$balance.' OR commission_rules.value_to IS NULL) 
               ');
    }
}
