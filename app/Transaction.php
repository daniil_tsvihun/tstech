<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

abstract class Transaction extends Model
{
    abstract function create();

    /**
     * Get only active transaction
     * @param $query
     * @return mixed
     */
    public static function scopeActive($query)
    {
        return $query->where('status_id','=',1);
    }

    /**
     * Get transactions only by past months
     * @param $query
     * @return mixed
     */
    public static function scopeExceptCurrentMonth($query)
    {
        return $query->whereRaw('(MONTH(NOW())!=MONTH(transaction_timestamp) OR (MONTH(NOW())=MONTH(transaction_timestamp) AND YEAR(NOW())!=YEAR(transaction_timestamp)))');
    }

    /**
     * Getting sum of transaction`s list
     * @return mixed
     */
    public static function getSum()
    {
        return self::selectRaw('(CASE WHEN sum(`value`) IS NULL THEN 0 ELSE sum(`value`) END) as sum')->active();
    }
}
