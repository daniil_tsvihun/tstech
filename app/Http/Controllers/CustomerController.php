<?php

namespace App\Http\Controllers;

use App\{Customer,Account};
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::all()->all();
        return view('customers', compact('customers'));
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        $accounts=$customer->accounts->all();
        return view('acc_details', compact('accounts','customer'));

    }

}
