<?php

namespace App\Http\Controllers;

use App\{Account,Debit,Credit};

class AccountController extends Controller
{

    private $commission_day=1;

    /**
     * Take all deposits that should be commissioned and earn commissions
     */
    public function commission()
    {
        if(date('d')==$this->commission_day){
            $commissions=Account::getCommissionOnDeposits()->get();
            foreach($commissions as $commission){
                $debit=new Debit;
                $debit->value=$commission->commission;
                $debit->account_id=$commission->id;
                $debit->type_id=2;
                $debit->status_id=1;
                $debit->save();
            }
            echo json_encode($commissions);
        } else {
            echo json_encode(['error'=>'Non-commission day']);
        }
    }

    /**
     * Cron event for deposit interest creation
     * Take all deposit which should be interested in this day and create credits
     */
    public function capitalize()
    {
        $interests=Account::getInterestOnDeposits()->get();

        foreach($interests as $interest){
            $newinterest=new Credit;
            $newinterest->value=$interest->interest;
            $newinterest->account_id=$interest->id;
            $newinterest->type_id=2;
            $newinterest->status_id=1;
            $newinterest->save();
        }
        echo json_encode($interests);

    }

}
