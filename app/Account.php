<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    const TABLE_NAME='account';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table=$this::TABLE_NAME;
    }

    /**
     * Getting only deposit accounts
     * @param $query
     * @return mixed
     */
    public static function scopeDeposit($query)
    {
        return $query->where('account_type','=',1);
    }

    /**
     * Getting account with balance
     * @param $conditions array
     * @return mixed
     */
    public static function getBalance($conditions=[])
    {
        $debit=Debit::getSum()->whereRaw('account_id='.self::TABLE_NAME.'.id');
        $credit=Credit::getSum()->whereRaw('account_id='.self::TABLE_NAME.'.id');
        if(in_array('exceptCurrentMonth',$conditions)){
            $debit=$debit->exceptCurrentMonth();
            $credit=$credit->exceptCurrentMonth();
        }
        $balance=Account::selectRaw('account.*,(('.$credit->toSql().')-('.$debit->toSql().')) as balance')
            ->mergeBindings($debit->getQuery())
            ->mergeBindings($credit->getQuery());

        if(in_array('exceptAlreadyCommissioned',$conditions)){
            //Get accounts only without commission in this month
            $commission_debit=Debit::whereRaw('account_id='.self::TABLE_NAME.'.id AND status_id=1 AND type_id=2')->selectRaw('count(*)');
            $balance=$balance->whereRaw('('.$commission_debit->toSql().')=0')->mergeBindings($commission_debit->getQuery());
        }
        if(in_array('exceptAlreadyInterested',$conditions)){
            //Get accounts only without commission in this month
            $interes_credit=Credit::whereRaw('account_id='.self::TABLE_NAME.'.id AND status_id=1 AND type_id=2')->selectRaw('count(*)');
            $balance=$balance->whereRaw('('.$interes_credit->toSql().')=0')->mergeBindings($interes_credit->getQuery());
        }
        return $balance;
    }

    /**
     * Calculation deposit commissions for each deposit`s account balance
     * @return [[account.id,balance,commission],...]
     */
    public static function getCommissionOnDeposits()
    {
        /*
         * Getting balance only for deposit accounts
         */
        $getBalance=Account::getBalance(['exceptCurrentMonth','exceptAlreadyCommissioned'])->deposit();
        $getBalanceAlias='acc_bal';
        /*
         * Subquery for commission calculation
         *
         */
        $calcCommission=CommissionRule::calcCommission($getBalanceAlias.'.balance');
        $commissions = DB::table(
            DB::raw('('.$getBalance->toSql().') '.$getBalanceAlias)
        )
            ->selectRaw(
                $getBalanceAlias.'.id,'.
                $getBalanceAlias.'.balance,
                (CASE
                  WHEN
                    MONTH(acc_bal.created_on) = MONTH(DATE_ADD(NOW(), INTERVAL -1 MONTH)) 
                    AND 
                    YEAR(acc_bal.created_on) = YEAR(NOW())
                  THEN
                    ROUND(('.$calcCommission->toSql().') / DAY(created_on),2)
                  ELSE
                    ('.$calcCommission->toSql().') 
                END ) as commission'
            )
            ->mergeBindings($calcCommission->getQuery())
            ->mergeBindings($getBalance->getQuery());
        /*
        ***WHOLE RAW QUERY

            SELECT
              acc_bal.id,
              acc_bal.balance,
              CASE
                WHEN MONTH(acc_bal.created_on) = MONTH(DATE_ADD(NOW(), INTERVAL -1 MONTH)) AND YEAR(acc_bal.created_on) = YEAR(NOW())
              THEN
                ROUND((
                  SELECT CASE
                         WHEN cr.min_commission > ROUND(acc_bal.balance * cr.percentage / 100, 2)
                           THEN cr.min_commission
                         WHEN cr.max_commission IS NOT NULL AND cr.max_commission < ROUND(acc_bal.balance * cr.percentage / 100, 2)
                           THEN cr.max_commission
                         ELSE ROUND(acc_bal.balance * cr.percentage / 100, 2)
                         END
                  FROM commission_rules cr
                  WHERE value_from <= acc_bal.balance AND (value_to >= acc_bal.balance OR value_to IS NULL)
                ) / DAY(created_on),2)
              ELSE
              (
                SELECT CASE
                       WHEN cr.min_commission > ROUND(acc_bal.balance * cr.percentage / 100, 2)
                         THEN cr.min_commission
                       WHEN cr.max_commission IS NOT NULL AND cr.max_commission < ROUND(acc_bal.balance * cr.percentage / 100, 2)
                         THEN cr.max_commission
                       ELSE ROUND(acc_bal.balance * cr.percentage / 100, 2)
                       END
                FROM commission_rules cr
                WHERE value_from <= acc_bal.balance AND (value_to > acc_bal.balance OR value_to IS NULL)

              )
              END AS comission
            FROM (
                   SELECT
                     account.*,
                     ((select (CASE WHEN sum(`value`) IS NULL THEN 0 ELSE sum(`value`) END) as sum from `credit` where `status_id` = 1 and account_id=account.id and (MONTH(NOW())!=MONTH(transaction_timestamp) OR (MONTH(NOW())=MONTH(transaction_timestamp) AND YEAR(NOW())!=YEAR(transaction_timestamp))))
                      -
                      (select (CASE WHEN sum(`value`) IS NULL THEN 0 ELSE sum(`value`) END) as sum from `debit` where `status_id` = 1 and account_id=account.id and (MONTH(NOW())!=MONTH(transaction_timestamp) OR (MONTH(NOW())=MONTH(transaction_timestamp) AND YEAR(NOW())!=YEAR(transaction_timestamp))))
                     ) as balance
                   FROM account
                   WHERE account.status_id = 1
                         AND account.account_type = 1
                 ) acc_bal;

        */
        return $commissions;
    }

    /**
     * Calculate interests for deposits
     * @return mixed
     */
    public static function getInterestOnDeposits()
    {
        /*
        * Getting balance only for deposit accounts
        */
        $getBalance=Account::getBalance(['exceptCurrentMonth','exceptAlreadyInterested'])->deposit();
        $getBalanceAlias='acc_bal';
        $interest = DB::table(
            DB::raw('('.$getBalance->toSql().') '.$getBalanceAlias)
        )
            ->selectRaw(
                $getBalanceAlias.'.id,'.
                $getBalanceAlias.'.balance,
                CASE
                    WHEN balance<=0 THEN 0
                  ELSE ROUND((balance*percentage/100),2)
                END as interest
                '
            )
            ->crossJoin('deposit_to_account','deposit_to_account.account_id','=',$getBalanceAlias.'.id')
            ->crossJoin('deposit_conditions','deposit_conditions.id','=','deposit_to_account.condition_id')
            //CHECK IF THE CURRENT DAY IS deposit creation day
            ->whereRaw('(
                  CASE WHEN DAY(created_on)>DAY(LAST_DAY(NOW()))
                    THEN DAY(LAST_DAY(NOW()))
                    ELSE DAY(NOW())
                  END
                    )=DAY(NOW())')
            ->mergeBindings($getBalance->getQuery());

        return $interest;
    }

}
