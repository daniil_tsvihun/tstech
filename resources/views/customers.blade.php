<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Customers</title>
</head>
<body>
<h1>Customers</h1>
<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Surname</th>
            <th>Status</th>
            <th>Accounts</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($customers as $customer): ?>
            <tr>
                <td><?php echo $customer->id; ?></td>
                <td><?php echo $customer->name; ?></td>
                <td><?php echo $customer->surname; ?></td>
                <td><?php echo $customer->status; ?></td>
                <td>
                    <a href="/customer/<?php echo $customer->id; ?>">Show accounts</a>
                </td>

            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<style>

</style>
</body>
</html>