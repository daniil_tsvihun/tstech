<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Customers</title>
</head>
<body>
<h1><?php echo $customer->name.' '.$customer->surname; ?></h1>
<table>
    <thead>
    <tr>
        <th>ID</th>
        <th>Created On</th>
        <th>Status</th>
        <th>Show details</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($accounts as $account): ?>
    <tr>
        <td><?php echo $account->id; ?></td>
        <td><?php echo $account->created_on; ?></td>
        <td><?php echo $account->status_id; ?></td>
        <td>
            <a href="/account/<?php echo $account->id; ?>">Show account details</a>
        </td>

    </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<style>

</style>
</body>
</html>