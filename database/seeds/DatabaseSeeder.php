<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CustomersSeeder::class);
        $this->call(AccountSeeder::class);
        $this->call(CustomerStatusSeeder::class);
        $this->call(AccountStatusSeeder::class);
        $this->call(CreditSeeder::class);
        $this->call(DebitSeeder::class);
        $this->call(TransactionStatusSeeder::class);
        $this->call(CommissionRulesSeeder::class);
        $this->call(DepositConditionsSeeder::class);
        $this->call(DepositToAccountSeeder::class);
        $this->call(TransactionStatusSeeder::class);
        $this->call(DebitTypeSeeder::class);
    }
}
