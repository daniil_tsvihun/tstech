<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepositConditionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('deposit_conditions')->insert([
            ['percentage' => '10.5'],
            ['percentage' => '13'],
            ['percentage' => '5'],

        ]);
    }
}
