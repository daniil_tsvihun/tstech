<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommissionRulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('commission_rules')->insert([
            ['value_from' => 0,'value_to'=>1000,'percentage' => 5,'min_commission'=>50],
            ['value_from' => 1000,'value_to'=>10000,'percentage' => 6,'min_commission'=>0],
            ['value_from' => 10000,'value_to'=>null,'percentage' => 7,'max_commission'=>5000],
        ]);
    }
}
