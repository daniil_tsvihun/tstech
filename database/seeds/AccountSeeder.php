<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('account')->insert([
            ['customer_id' => 1,'account_type'=>1,'created_on' => '2017.01.14','status_id'=>1],
            ['customer_id' => 1,'account_type'=>1,'created_on' => '2017.01.14','status_id'=>1],
            ['customer_id' => 2,'account_type'=>1,'created_on' => '2017.01.14','status_id'=>1],
            ['customer_id' => 3,'account_type'=>1,'created_on' => '2017.01.14','status_id'=>1],
            ['customer_id' => 3,'account_type'=>1,'created_on' => '2017.01.14','status_id'=>1],

        ]);
    }
}
