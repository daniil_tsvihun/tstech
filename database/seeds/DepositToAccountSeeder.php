<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepositToAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('deposit_to_account')->insert([
            ['account_id' => 1,'condition_id'=>1],
            ['account_id' => 2,'condition_id'=>2],
            ['account_id' => 3,'condition_id'=>3],
            ['account_id' => 4,'condition_id'=>2],
            ['account_id' => 5,'condition_id'=>3],
        ]);
    }
}
