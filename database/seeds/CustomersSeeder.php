<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            ['inn' => '1234567890','name'=>'Vasylyi','surname'=>'Pupkin','gender'=>'m','birthdate'=>'1970.01.12','status'=>1],
            ['inn' => '0987654321','name'=>'Petro','surname'=>'Sagaydak','gender'=>'m','birthdate'=>'1990.12.09','status'=>1],
            ['inn' => '1235434567','name'=>'Katherina','surname'=>'Jhons','gender'=>'f','birthdate'=>'1985.01.12','status'=>1],
            ['inn' => '3574848392','name'=>'Serg','surname'=>'Travolta','gender'=>'m','birthdate'=>'1989.01.12','status'=>0],
        ]);
    }
}
