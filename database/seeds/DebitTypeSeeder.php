<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DebitTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('debit_type')->insert([
            ['id'=>1,'description' => 'Normal'],
            ['id' => 2,'description'=>'Commission'],
        ]);
    }
}
