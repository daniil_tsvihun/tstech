<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreditSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('credit')->insert([
            ['value' => '23.50','account_id'=>1,'transaction_timestamp'=>'2017-12-18'],
            ['value' => '13.25','account_id'=>1,'transaction_timestamp'=>'2017-11-18'],
            ['value' => '9567','account_id'=>1,'transaction_timestamp'=>'2016-12-18'],
            ['value' => '10000','account_id'=>1,'transaction_timestamp'=>'2017-10-18'],
            ['value' => '987.10','account_id'=>2,'transaction_timestamp'=>'2017-12-16'],
            ['value' => '900','account_id'=>2,'transaction_timestamp'=>'2017-11-12'],
            ['value' => '10000.10','account_id'=>3,'transaction_timestamp'=>'2017-12-10'],
            ['value' => '1000','account_id'=>4,'transaction_timestamp'=>'2017-11-28'],
            ['value' => '2000','account_id'=>5,'transaction_timestamp'=>'2017-12-10'],
        ]);
    }
}
